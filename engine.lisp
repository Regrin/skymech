(ql:quickload 'ltk)

;****************************************
;        Математический аппарат         *
;****************************************

(defstruct sky-obj
  vis
  pos ;Вектор положения тела в пространстве
  dir ;Вектор движения
  mass)

(defun sq (n)
    (* n n))

(defun distance (A B)
    (abs (- (sky-obj-pos A)
            (sky-obj-pos B))))

(defun gravitation (A B)
    (/ (* (sky-obj-mass A)
          (sky-obj-mass B))
       (sq (distance A B))))

(defun sky-obj-next-moment (M S dt)      ; Функция берёт два объекта, подвижный и статичный, после чего
    (let ((dVm  (* (/ (gravitation S M)  ; после чего рассчитывает их гравитационное взаимодействие.
		              (distance    S M)  ; Переменная dt должна была увеличивать точность взаимодействия,
		              (sky-obj-mass  M)) ; но она влияет на траекторию движения. ИСПРАВИТЬ!!!
		           (- (sky-obj-pos S)
	                  (sky-obj-pos M)))))

        (make-sky-obj :pos (+ (* 1.0
                                 dt
                                 dVm)
		                      (sky-obj-pos M)
	                          (* dt
                                 (sky-obj-dir M)))
                      
		              :dir (+ (* 1.0
                                 dt
                                 dVm)
			                  (sky-obj-dir M))
                      
		              :mass (sky-obj-mass M))))

(defun finarfin (num fun lst)    ; Делает тоже, что и reduce только не обязательно с первого элемента
    (let ((elm (nth num lst)))   ; КАК ОПТИМИЗИРОВАТЬ?
        (reduce fun (cons elm (remove elm lst)))))

(defun space-next-moment (obj-list dt)            ; Вычисляет гравитационное взаимодействие каждого объекта с
    (loop for i from 0 to (1- (length obj-list))  ; каждым.
	      collect (finarfin i
                            #'(lambda (S M)
			                      (sky-obj-next-moment S M dt))
		                    obj-list)))

;(defun iter-space-next-moment (space dt)
;  )



(defun pos-x (vect)   ;Данная функция предназначена для двумерных векторов представленных
    (realpart vect))  ;комплексными числами

(defun pos-y (vect)   ;Данная функция предназначена для двумерных векторов представленных
    (imagpart vect))  ;комплексными числами

(defun sky-obj-pos-x (obj)
    (pos-x (sky-obj-pos obj)))

(defun sky-obj-pos-y (obj)
    (pos-y (sky-obj-pos obj)))

; *******************************************
; Загрузка файла
; *******************************************

; Хранить конфигурации планет будем в виде списков свойств:
; (:vis "A"
;  :pos #c(1 2)
;  :dir #c(2 3)
;  :mass 100)
; Сам космос будет представлен просто списком параметров планет

(defun parse-sky-obj (s-exp) ; s-exp в данном случае - параметры планет
    (make-sky-obj :vis  (getf s-exp :vis)
                  :pos  (getf s-exp :pos)
                  :dir  (getf s-exp :dir)
                  :mass (getf s-exp :mass)))

(defun parse-space (s-exp) ; s-exp в данном случае - список планет (космос)
    (mapcar #'parse-sky-obj s-exp))

(defun load-text-file (path) ; Как-то не нравится мне эта функция, надо или переписать или заменить стандартной
    (with-open-file (file path ;АХТУНГ Если подать на вход нулевую строку, возникает ошибка
                          :direction :input
                          :if-does-not-exist nil)
        (do ((string (read-line file nil 'eof)
                      (read-line file nil 'eof))
              (result "" (format nil "~A~A~%" result string)))
            ((eql string 'eof) result))))

(defun parse-space-from-string (conf-string)    ; Пока не буду делать сложную проверку конфигураций.
    (parse-space (read-from-string conf-string))) ; Будем считать, что они корректны


     
; *******************************************
; GUI
; *******************************************


(defun sky-obj-vis-init (obj canv)
    (if (stringp (sky-obj-vis obj))
        (ltk:create-text canv
		                 (sky-obj-pos-x obj)
		                 (sky-obj-pos-y obj)
		                 (sky-obj-vis obj))))

(defun space-vis-init (space canv)
    (loop for obj in space
          collect (sky-obj-vis-init obj canv)))

(defun space-vis-update (space spvis canv scale x-bias y-bias)
    (loop for obj in space
          for vis in spvis
	      do (let ((scaled-pos
		             (* (+ (sky-obj-pos obj)
			               (complex x-bias y-bias))
		                scale)))
	             (ltk:set-coords canv
			                     vis
			                     (list (pos-x scaled-pos)
				                       (pos-y scaled-pos))))))

(defun visualise (space &key (speed 0) (dt 1) (scale 1) (x-bias 0) (y-bias 0))
    (ltk:with-ltk ()
        (let* ((frame (make-instance 'ltk:frame))
	           (canv  (ltk:make-canvas frame
	                                   :width  1100
	                                   :height 1000))
	           (space-vis (space-vis-init space canv)))

            (ltk:grid frame 0 0)
            (ltk:grid canv  0 0)

            (labels ((next (space)
		                 (ltk:after speed
			                        #'(lambda ()
				                          (space-vis-update space space-vis canv scale x-bias y-bias)
				                          (next (space-next-moment space dt))))))
	            (next (space-next-moment space dt))))))

(defun main ()
    (ltk:with-ltk ()
        (let* (
               (control-frame (make-instance 'ltk:frame :width 100
                                                        :height 100))
               (text-frame (make-instance 'ltk:frame :width 100
                                                     :height 100))
               
               (speed-spinbox (make-instance 'ltk:spinbox
                                             :from 0 :to 10000
                                             :master control-frame))
               
               (text-field (make-instance 'ltk:scrolled-text :master text-frame
                                                             :width 1 ; АХТУНГ Высота и ширина меня совсем не слушаются и не
                                                             :height 1)) ; масштабируются при изменении размера окна
               (load-button  (make-instance 'ltk:button
                                            :text "Загрузить"
                                            :master control-frame
                                            :command #'(lambda ()
                                                           (let* ((path-to-space-config (ltk:get-open-file :filetypes '(("S-expression" ".sxp"))))
                                                                  (space-config (if (equal path-to-space-config "")
                                                                                    nil
                                                                                    (load-text-file path-to-space-config))))
                                                               (if space-config
                                                                   (setf (ltk:text text-field)
                                                                         space-config)))))) ;Проверка, был ли выбран файл сделана слишком мудрёно.
                                                                                            ;А без неё, если не выбрать файл, а закрыть диалог,
                                                                                            ;вылезет окошко с ошибкой. ПЕРЕПИСАТЬ
               

               (start-button (make-instance 'ltk:button
                                            :text "Старт"
                                            :master control-frame
                                            :command #'(lambda () ;АХТУНГ Если не загружен файл с космосом, вылетает окно с ошибкой.
                                                           (visualise (parse-space-from-string (ltk:text text-field))
                                                                      :speed (parse-integer (ltk:text speed-spinbox)))))))
            (ltk:grid text-frame 0 0)
            (ltk:grid text-field 0 0)
            (ltk:grid control-frame 1 0) (ltk:grid speed-spinbox 0 0)
            (ltk:grid load-button 1 0)
            (ltk:grid start-button 1 1))))
